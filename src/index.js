// BDD de départ

// BDD inéxistante dans localStorage => création
database = JSON.parse(localStorage.getItem("objDatabase"));
if (database == null) {
    var jsonDatabase =
        [
            {
                "selectedUser":
                {
                    "id": 0000,
                    "username": "",
                    "tasks": [],
                    "lastAddedId": 0002

                }
            },
            {
                "users":
                    [
                        {
                            "id": 0001,
                            "username": "DevThomas",
                            "tasks": [
                                {
                                    "idTask": 1,
                                    "priority": 'urgent',
                                    "todo": false,
                                    "description": "Coder c'est bien"
                                },
                                {
                                    "idTask": 2,
                                    "priority": 'urgent',
                                    "todo": false,
                                    "description": "Acheter des fleurs pour ma femme"
                                },
                                {
                                    "idTask": 3,
                                    "priority": 'normal',
                                    "todo": true,
                                    "description": "Coder à la plage"
                                }
                            ],
                            "lastUserTaskId": 3
                        },
                        {
                            "id": 0002,
                            "username": "DevChaambane",
                            "tasks": [
                                {
                                    "idTask": 1,
                                    "priority": 'normal',
                                    "todo": false,
                                    "description": "Faire les courses"
                                },
                                {
                                    "idTask": 2,
                                    "priority": 'urgent',
                                    "todo": true,
                                    "description": "Revoir le Python"
                                },
                                {
                                    "idTask": 3,
                                    "priority": 'normal',
                                    "todo": false,
                                    "description": "Sql c'est light"
                                }
                            ],
                            "lastUserTaskId": 3
                        }
                    ]
            }
        ];
    localStorage.setItem('objDatabase', JSON.stringify(jsonDatabase));
    var database = JSON.parse(localStorage.objDatabase);

// BDD éxistante dans localStorage => initialisation
} else {
    database = JSON.parse(localStorage.getItem("objDatabase"));
}

// Affichage des utilisateurs dans les options avec select_________________________________________
let objJson = JSON.parse(localStorage.getItem("objDatabase"));
let html = `<option selected hidden>Utilisateur</option>`;
let segment = '';
for (var i = 0; i < database[1].users.length; i++) {
    segment = `<option id="${database[1].users[i].id}" class="user-name">${database[1].users[i].username}</option>`;
    html += segment;
}
const userList = document.querySelector("#user-list");
userList.innerHTML = html;
html = '';
segment = '';
userNameEvent();


// Actions sur l'utilisateur selectionné___________________________________________________________
const taskTodo = document.querySelector('#task-todo');
const taskDone = document.querySelector('#task-done');

function userNameEvent() {
    const userNameH1 = document.querySelector("#user-name");
    document.querySelectorAll('.user-name').forEach(function (item) {
        item.addEventListener("click", function () {
            taskTodo.innerHTML = '';
            getDataOneUser(item.id);
            userNameH1.innerText = item.innerText;
            // Utilisateur sélectionné => en BDD
            database[0].selectedUser.id = item.id;
            localStorage.setItem("objDatabase", JSON.stringify(database));
            // Affichage des boutons de commande cachés
            document.querySelector('#deleteUser').hidden = false;
            document.querySelector('#hidden').style.visibility = 'visible';
        })
    })
};

// Récupération des tâches de l'utilisateur
function getDataOneUser(id) {
    var htmlTodo = '';
    var htmlDone = '';
    for (var i = 0; i < database[1].users.length; i++) {
        if (database[1].users[i].id == id) {

            database[1].users[i].tasks.forEach(function (task) {
                if (task.todo === true) {
                    let segment = `
                    <div class="task-row">
                        <textarea id="${task.idTask}" class="input-text ${task.priority}">${task.description}</textarea>
                        <input type="checkbox" class="checkbox">
                    </div>`;
                    htmlTodo += segment;
                } else if (task.todo === false) {
                    let segment = `
                    <div class="task-row">
                        <input type="checkbox" class="checkbox">
                        <textarea id="${task.idTask}" class="input-text ${task.priority}">${task.description}</textarea>
                    </div>`;
                    htmlDone += segment;
                };
            });
        };
    };
    taskTodo.innerHTML = htmlTodo;
    taskDone.innerHTML = htmlDone;
    html = '';
    document.querySelectorAll('textarea').forEach(function (elem) {
        addValidationTaskEvent(elem);
    });
};


// Adding user in db with popup____________________________________________________________________
const addUserBtn = document.querySelector('.add-username');
const addUser = document.querySelector('.add');
const addUserForm = document.querySelector('.add form');

addUserBtn.addEventListener('click', () => {
    addUser.classList.add("active");
    addUserForm.addEventListener('submit', (e) => {
        e.preventDefault();

        // récupération du dernier id utilisateur crée
        let nextId = (database[0].selectedUser.lastAddedId + 1);
        // redéfinition du dernier id crée
        database[0].selectedUser.lastAddedId = nextId;

        // structure nouvel utilisateur
        userName = addUserForm.username.value;
        let newUser =
        {
            "id": nextId,
            "username": userName,
            "tasks": [],
            "lastUserTaskId": 0
        };

        database[1].users.push(newUser);
        localStorage.setItem("objDatabase", JSON.stringify(database));

        addUser.classList.remove('active');
        addUserForm.reset();
        // refresh liste utilisateur + actions associées
        window.location.reload(true);
    });
});

// Remove popup
const popup = document.querySelector('.popup');
window.addEventListener('click', (e) => {
    if (e.target == popup) {
        popup.classList.remove('active');
        addUserForm.reset();
    }
});


// Suppression d'un utilisateur____________________________________________________________________
const deleteUser = document.querySelector('#deleteUser');
deleteUser.addEventListener("click", () => {
    // id de utilisateur sélectionné
    let selectedUserId = database[0].selectedUser.id;
    // prochain id de tâche de l'utilisateur
    for (var i = 0; i < database[1].users.length; i++) {
        if (database[1].users[i].id == selectedUserId) {
            database[1].users.splice(i, 1);
            localStorage.setItem("objDatabase", JSON.stringify(database));
            // refresh liste utilisateur
            window.location.reload(true);
            break
        }
    }
});


// Ajout d'une tâche (champ saisie) avec couleur (priorité)______________________________________________________
const taskPriority = [document.querySelector(".normal"), document.querySelector(".urgent")];
const taskCategorie = document.querySelector('.task-categorie');

taskPriority.forEach(function (item) {
    if (item.innerHTML === 'Normale') {
        var priority = 'normal';
    } else if (item.innerHTML === 'Urgente') {
        var priority = 'urgent';
    };
    item.addEventListener('click', () => {
        item.parentNode.selectedIndex = 0;
        addTasks(priority);
    });
});

const newTaskText = 'Nouvelle tâche.';

function addTasks(priority) {
    // id de utilisateur sélectionné
    let selectedUserId = database[0].selectedUser.id;
    // prochain id de tâche de l'utilisateur
    for (var i = 0; i < database[1].users.length; i++) {
        if (database[1].users[i].id == selectedUserId) {
            var nextUserTaskId = database[1].users[i].lastUserTaskId + 1;
            database[1].users[i].lastUserTaskId = nextUserTaskId;
            break
        }
    }
    let html = '';
    if (priority === 'normal') {
        // future modification du DOM
        html = `<textarea
                    id="${nextUserTaskId}" class="input-text normal">${newTaskText}</textarea>
              <input type="checkbox" class="checkbox">`;
        // future tâche en BDD
        var newTask = {
            "idTask": nextUserTaskId,
            "priority": 'normal',
            "todo": true,
            "description": ""
        };
    } else if (priority === 'urgent') {
        html = `<textarea
                    id="${nextUserTaskId}" class="input-text urgent">${newTaskText}</textarea>
              <input type="checkbox" class="checkbox">`;
        var newTask = {
            "idTask": nextUserTaskId,
            "priority": 'urgent',
            "todo": true,
            "description": ""
        };
    }
    // ajout tâche de l'utilisateur en BDD
    for (var i = 0; i < database[1].users.length; i++) {
        if (database[1].users[i].id == selectedUserId) {
            database[1].users[i].tasks.push(newTask);
            localStorage.setItem("objDatabase", JSON.stringify(database));
            break
        }
    }
    // modification du DOM
    let elem = document.createElement('div');
    elem.innerHTML = html;
    elem.setAttribute('class', 'task-row');
    taskTodo.insertBefore(elem, taskTodo.firstChild);
    addValidationTaskEvent(elem.firstChild);
};


// Valider la saisie d'une tache en cliquant en dehors (à côté)____________________________________
function addValidationTaskEvent(elem) {
    elem.addEventListener("click", function () {
        // suppression contenu textarea des nouvelles tâches au clic
        if (elem.innerHTML === newTaskText) { elem.innerText = '' } ;
        // message d'aide à la validation de la saisie => zone #message
        document.querySelector('#message').innerHTML = '<p>Astuce: cliquez en dehors de la zone de saisie pour valider</p>';
        // valider saisie si clic en dehors textarea
        document.addEventListener("click", validateTask);
        function validateTask() {
            let targetEl = event.target; // élément cliqué    
            do {
                if (targetEl == elem) {
                    // click interne
                    return;
                }
                targetEl = targetEl.parentNode;

                // id de utilisateur sélectionné
                let selectedUserId = database[0].selectedUser.id;
                // id de la tâche à validée (html)
                let idTaskToValidate = elem.id;
                // recherche de l'utilisateur dans BDD
                for (var i = 0; i < database[1].users.length; i++) {
                    if (database[1].users[i].id == selectedUserId) {
                        // recherche de l'id de la tâche validée dans BDD
                        for (var y = 0; y < database[1].users[i].tasks.length; y++) {
                            if (database[1].users[i].tasks[y].idTask == idTaskToValidate) {
                                database[1].users[i].tasks[y].description = elem.value;
                                localStorage.setItem("objDatabase", JSON.stringify(database));
                                break
                            }
                        }
                        break
                    }
                }
                // Messagede de validation de la saisie
                document.querySelector('#message').innerHTML = 'Saisie Validée !';
                // Suppression du messagede de validation
                setTimeout(function () { document.querySelector('#message').innerHTML = '' }, 1500);
                
            } while (targetEl);
            // Suppression évènement du clic de validation (sinon se répète)
            document.removeEventListener("click", validateTask);
        };
    });
};


// Supprimer textarea si checked => clic bouton supprimer__________________________________________
const delTasks = document.querySelector('#delTasks');
delTasks.addEventListener("click", () => {
    deleteTasks();
});
function deleteTasks() {
    document.querySelectorAll('.checkbox:checked').forEach(function (item) {
        //shake
        item.parentElement.classList.add("apply-shake");
        item.parentElement.addEventListener("animationend", (e) => {
            item.parentElement.remove();
        });

        // Actions sur la BDD
        // id de la tâche à supprimer (html)
        if (taskTodo.contains(item)) {
            var idTaskToDelete = item.previousElementSibling.id;
        } else if (taskDone.contains(item)) {
            var idTaskToDelete = item.nextElementSibling.id;
        }
        // recherche de l'utilisateur dans BDD
        let selectedUserId = database[0].selectedUser.id;
        for (var i = 0; i < database[1].users.length; i++) {
            if (database[1].users[i].id == selectedUserId) {
                // recherche de l'id de la tâche validée dans BDD
                for (var y = 0; y < database[1].users[i].tasks.length; y++) {
                    if (database[1].users[i].tasks[y].idTask == idTaskToDelete) {
                        database[1].users[i].tasks.splice(y, 1);
                        localStorage.setItem("objDatabase", JSON.stringify(database));
                        break
                    }
                }
                break
            }
        }
    });
};


// Filtrage des tâches_____________________________________________________________________________
const taskFilter = [document.querySelector(".noFilter"), document.querySelector(".normalFilter"),
document.querySelector(".urgentFilter")];
taskFilter.forEach(function (item) {
    if (item.innerHTML === 'Tout') {
        var filter = 'all';
    } else if (item.innerHTML === '| Normales') {
        var filter = 'normals';
    } else if (item.innerHTML === '| Urgentes') {
        var filter = 'urgents';
    };
    item.addEventListener('click', () => {
        item.parentNode.selectedIndex = 0;
        tasksFilter(filter);
    });
});

function tasksFilter(filter) {
    if (filter === 'all') {
        document.querySelectorAll('.normal, .urgent').forEach(function (item) {
            item.parentElement.style.display = "flex";
        });
    } else if (filter === 'normals') {
        document.querySelectorAll('.urgent').forEach(function (item) {
            item.parentElement.style.display = "none";
        });
        document.querySelectorAll('.normal').forEach(function (item) {
            item.parentElement.style.display = "flex";
        });
    } else if (filter === 'urgents') {
        document.querySelectorAll('.normal').forEach(function (item) {
            item.parentElement.style.display = "none";
        });
        document.querySelectorAll('.urgent').forEach(function (item) {
            item.parentElement.style.display = "flex";
        });
    }
};


// Basculement [>] et [<] des tâches_______________________________________________________________
// Définitions des boutons de basculement [>] et [<]
const doneTask = document.querySelector('#doneTask');
const todoTask = document.querySelector('#todoTask');
// Liste de taches à faire (gauche) et faites (droite)
const todoList = document.querySelector('#todoList');
const doneList = document.querySelector('#doneList');
// Zones insertion pour 'insertafter'
let insertTodo = document.querySelector('#task-todo');
let insertDone = document.querySelector('#task-done');
// Appels des fonctions au clics des boutons [>] et [<]
doneTask.addEventListener("click", () => {
    doneTasks();
});
todoTask.addEventListener("click", () => {
    todoTasks();
});
// Fonctions de déplacement des taches (textarea + checbox => task-row)
function doneTasks() {
    todoList.querySelectorAll('.checkbox:checked').forEach(function (item) {
        // id de la tâche à supprimer (html)
        if (taskTodo.contains(item)) {
            var idTaskToModify = item.previousElementSibling.id;
        } else if (taskDone.contains(item)) {
            var idTaskToModify = item.nextElementSibling.id;
        }
        // recherche de l'utilisateur dans BDD
        let selectedUserId = database[0].selectedUser.id;
        for (var i = 0; i < database[1].users.length; i++) {
            if (database[1].users[i].id == selectedUserId) {
                // recherche de l'id de la tâche dans BDD
                for (var y = 0; y < database[1].users[i].tasks.length; y++) {
                    if (database[1].users[i].tasks[y].idTask == idTaskToModify) {

                        database[1].users[i].tasks[y].todo = false;
                        localStorage.setItem("objDatabase", JSON.stringify(database));
                        break
                    }
                }
                break
            }
        }
        // modif du DOM
        let task = item.parentElement;
        task.removeChild(item);
        task.prepend(item);
        let elem = document.createElement('div');
        elem.setAttribute('class', 'task-row');
        elem.innerHTML = task.innerHTML;
        insertDone.appendChild(elem);
        task.remove();
        addValidationTaskEvent(elem.lastElementChild);
    });
};
function todoTasks() {
    doneList.querySelectorAll('.checkbox:checked').forEach(function (item) {
        // id de la tâche à supprimer (html)
        if (taskTodo.contains(item)) {
            var idTaskToModify = item.previousElementSibling.id;
        } else if (taskDone.contains(item)) {
            var idTaskToModify = item.nextElementSibling.id;
        }
        // recherche de l'utilisateur dans BDD
        let selectedUserId = database[0].selectedUser.id;
        for (var i = 0; i < database[1].users.length; i++) {
            if (database[1].users[i].id == selectedUserId) {
                // recherche de l'id de la tâche dans BDD
                for (var y = 0; y < database[1].users[i].tasks.length; y++) {
                    if (database[1].users[i].tasks[y].idTask == idTaskToModify) {

                        database[1].users[i].tasks[y].todo = true;
                        localStorage.setItem("objDatabase", JSON.stringify(database));
                        break
                    }
                }
                break
            }
        }
        // modif du DOM
        let task = item.parentElement;
        task.removeChild(item);
        task.appendChild(item);
        let elem = document.createElement('div');
        elem.setAttribute('class', 'task-row');
        elem.innerHTML = task.innerHTML;
        insertTodo.appendChild(elem);
        task.remove();
        addValidationTaskEvent(elem.firstElementChild);
    });
};


// Afficher console.log BDD
let objJson3 = JSON.parse(localStorage.getItem("objDatabase"));
console.log(objJson3);